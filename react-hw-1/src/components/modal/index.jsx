import { Component } from "react";
import styles from "./Modal.module.scss";

class Modal extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div className={styles.modal} onClick={this.props.closeButtonHandler()}>
        <div
          className={styles.window}
          onClick={(event) => event.stopPropagation()}
        >
          <div className={styles.header}>
            <h3 className={styles.headerTitle}>{this.props.header}</h3>
            {this.props.closeButton && (
              <svg
                onClick={this.props.closeButtonHandler()}
                className={styles.closeBtn}
                width="22"
                height="22"
                viewBox="0 0 22 22"
                fill="none"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path
                  d="M21.0982 2.31565C21.4903 1.92671 21.4928 1.29356 21.1039 0.90145C20.715 0.509345 20.0818 0.506776 19.6897 0.895713L11.006 9.50921L2.46753 0.901438C2.07859 0.509339 1.44543 0.506781 1.05333 0.895724C0.661229 1.28467 0.658671 1.91783 1.04761 2.30993L9.58609 10.9177L0.901816 19.5318C0.50971 19.9207 0.507142 20.5539 0.896079 20.946C1.28502 21.3381 1.91818 21.3407 2.31028 20.9517L10.9946 12.3376L19.684 21.0975C20.0729 21.4896 20.7061 21.4922 21.0982 21.1032C21.4903 20.7143 21.4928 20.0811 21.1039 19.689L12.4145 10.9291L21.0982 2.31565Z"
                  fill="white"
                />
              </svg>
            )}
          </div>
          <p className={styles.text}>{this.props.text}</p>
          <div className={styles.actions}>{this.props.actions}</div>
        </div>
      </div>
    );
  }
}

export default Modal;
