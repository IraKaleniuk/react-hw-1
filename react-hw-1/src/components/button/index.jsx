import {Component} from "react";
import styles from "./Button.module.scss";

class Button extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <>
                <button className={styles.button}
                        data-modal-id={this.props.dataModalId}
                        style={{background: this.props.backgroundColor}}
                        onClick={this.props.onClick()}
                >
                    {this.props.text}
                </button>
            </>
        )
    }
}

export default Button;