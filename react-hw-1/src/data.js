export const buttonDeclarations = [
    {
        id: 0,
        dataModalId: "modalID1",
        backgroundColor: "#61dafb",
        text: "Open first modal"
    },
    {
        id: 1,
        dataModalId: "modalID2",
        backgroundColor: "#ffb13c",
        text: "Open second modal"
    }
];

export const modalWindowDeclarations = [
    {
        id: "modalID1",
        header: "Do you want to delete this file?",
        text: "Once you delete this file, it won’t be possible to undo this action. Are you sure you want to delete it?",
        closeButton: true,
        actions: [
            {
                text: "Ok",
                backgroundColor: "#b3382c",
            },
            {
                text: "Cancel",
                backgroundColor: "#b3382c",
            }
        ]
    },
    {
        id: "modalID2",
        header: "This site uses cookie!",
        text: "Cookies collect information about your preferences and your devices.",
        closeButton: false,
        actions: [
            {
                text: "Reject All",
                backgroundColor: "#b3382c",
            },
            {
                text: "Accept All",
                backgroundColor: "#b3382c",
            }
        ]
    }
]

