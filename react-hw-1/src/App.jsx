import Button from "./components/button";
import "./App.scss";
import styles from "./App.module.scss";
import Modal from "./components/modal";
import { Component } from "react";
import { modalWindowDeclarations as modalsData } from "./data";
import { buttonDeclarations as buttonsData } from "./data";

class App extends Component {
  constructor(props) {
    super(props);
    this.state = { openModal: "" };
  }

  setModalID = (event) => {
    const modalID = event.target.getAttribute("data-modal-id");
    this.setState((state) => ({ openModal: modalID }));
  };

  openModal = () => {
    const modalData = modalsData.filter(
      (modal) => modal.id === this.state.openModal
    )[0];

    const actions = modalData.actions.map(({ backgroundColor, text }) => (
      <Button
        key={text}
        backgroundColor={backgroundColor}
        text={text}
        onClick={() => this.onActionClick}
      />
    ));
    return (
      <Modal
        {...modalData}
        key={modalData.id}
        closeButtonHandler={() => this.closeModal}
        actions={actions}
      />
    );
  };

  closeModal = () => {
    this.setState(() => ({ openModal: "" }));
  };

  onActionClick = (event) => {
    console.log(event.target.textContent);
    this.setState(() => ({ openModal: "" }));
  };

  render() {
    const buttons = buttonsData.map(
      ({ dataModalId, backgroundColor, text, id }) => (
        <Button
          key={id}
          dataModalId={dataModalId}
          backgroundColor={backgroundColor}
          text={text}
          onClick={() => this.setModalID}
        />
      )
    );

    return (
      <>
        <div className={styles.buttonContainer}>
          {this.state.openModal !== "" ? this.openModal() : null}
          {buttons}
        </div>
      </>
    );
  }
}

export default App;
